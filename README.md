# Description
A tool for downloading guitar tabs from [ultimate-guitar](https://ultimate-guitar.com) and saving them with pretty names.

    » python3 main.py https://tabs.ultimate-guitar.com/p/pantera/cemetery_gates_ver3_tab.htm
    Downloading webpage...
    Tab wrote to: Cemetery Gates - Pantera.txt sucessfully.

# Usage
Running the script with the `--help` or `-h` will tell you all you need to know, but here's a quick rundown.

## Positional arguments
This script takes two positional arguments, page URL and output file path. Only the page URL is required, if no output file is specified the script will name your file for you as a `.txt` in the current directory.

## Flags
`-q` or `--quiet` will print nothing except the overwrite file prompt if a file with the specified name already exists.

`-h` or `--help` will print the help message.
