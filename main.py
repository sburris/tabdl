import re
import os
import argparse
import requests
from lxml import html

# Argument parsing
parser = argparse.ArgumentParser(description="Tool for saving tabs from ultimate-guitar.com")
parser.add_argument("url", help="tab page URL")
parser.add_argument("file", nargs="?", help="output file")
parser.add_argument("-q", "--quiet", action="store_true", help="will not print anything except file overwrite prompt.")
args = parser.parse_args()


# Functions
def output(message):
    if not args.quiet:
        print(message)


def exit_with_error(message):
    output(message + ", exiting...")
    raise SystemExit


# Get the page and ascertain that shit's on the level
output("Downloading webpage...")
page = requests.get(args.url)

if (page.status_code != requests.codes.ok):
    exit_with_error("Could not get page, HTTP error code: " + str(page.status_code))

# Extract useful file contents
tree = html.fromstring(page.content)
tab = tree.xpath('//pre[@class="js-tab-content js-init-edit-form js-copy-content js-tab-controls-item"]/text()')[0]

# Create a file name if one was not passed
if args.file is None:
    title = tree.xpath('.//title/text()')[0].lower()
    if "chords" in title:
        exit_with_error("This tool doesn't support chords yet")
    else:
        filename = title.replace(" tab", '').replace(" @ ultimate-guitar.com", '')
        filename = re.compile("\(?ver \d+\)? ").sub('', filename)
        filename = filename.title() + ".txt"
else:
    filename = args.file

# Check for overwriting then save the file
if os.path.exists(filename) and input("File exists, overwrite? [y/N] ").lower() != 'y':
    exit_with_error("Did not write file")
else:
    try:
        with open(filename, 'w+') as f:
            f.write(tab)
            f.close()
            output("Tab wrote to: " + filename + " successfully.")
    except:
        exit_with_error("Could not write to file")
